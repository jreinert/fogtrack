require "http/client"
require "./request_error"
require "./api_error"
require "./api_requests/*"
require "./client"

module Fogtrack
  class API
    BASE_PATH = "/f/api/0/jsonapi"

    def login(email, password)
      request(APIRequests::Login.new(email, password))
    end

    def list_intervals(from = nil, to = nil, unique = false)
      request(APIRequests::ListIntervals.new(from, to))
    end

    def start_tracking(case_number)
      request(APIRequests::StartTracking.new(case_number))
    end

    def stop_tracking
      request(APIRequests::StopTracking.new)
    end

    private def request(request : APIRequest)
      {% unless flag?(:release) %}
        STDERR.puts "POST #{BASE_PATH} - #{request.to_json}"
      {% end %}

      Fogtrack.client.post(BASE_PATH, body: request.to_json) do |response|
        parsed_response = request.parse_response(response.body_io)
        error_code = parsed_response.error_code
        unless error_code.nil?
          raise RequestError.new(error_code, parsed_response.errors)
        end

        parsed_response.data
      end
    end
  end
end
