require "../api_request"

module Fogtrack::APIRequests
  class Login < APIRequest
    @email : String
    @password : String

    def initialize(@email, @password)
      super("logon")
    end

    def parse_response(body)
      APIResponse(ResponseData).from_json(body)
    end

    # Empty data
    class ResponseData
      include JSON::Serializable

      getter! token : String?
    end
  end
end
